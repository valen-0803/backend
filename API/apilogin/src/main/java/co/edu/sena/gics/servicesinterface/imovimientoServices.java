package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.movimiento;

public interface imovimientoServices {
	
	public List<movimiento> findAll();
	
	public movimiento create(movimiento movimiento);
	
	public movimiento update(long id, movimiento movimiento);

	public void delete(long id);
	

}
