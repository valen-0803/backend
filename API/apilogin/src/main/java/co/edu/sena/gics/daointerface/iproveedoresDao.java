package co.edu.sena.gics.daointerface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.proveedores;

public interface iproveedoresDao extends CrudRepository<proveedores, Long>{

}
