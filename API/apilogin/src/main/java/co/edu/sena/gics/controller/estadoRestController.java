package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.estado;
import co.edu.sena.gics.servicesinterface.iestadoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class estadoRestController {
	
	@Autowired
	private iestadoServices estadoServices;
	
	@GetMapping("/estado")
	public List<estado> getAll(){
		return estadoServices.findAll();
	}
	
	@PostMapping("/estado")
	public estado create(@RequestBody estado estado) {
		System.out.println(estado.toString());
		return estadoServices.create(estado);
	}
	
	@PutMapping("/estado/{id}")
	public estado update(@PathVariable int id,@RequestBody estado estado ){
	System.out.println(id);
	System.out.println(estado);
	return this.estadoServices.update(id,estado);
	}

	@DeleteMapping("/estado/{id}")
	    public void delete(@PathVariable int id) {
	this.estadoServices.delete(id);

	}

}
