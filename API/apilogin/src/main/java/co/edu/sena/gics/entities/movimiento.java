package co.edu.sena.gics.entities;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="movimiento")

public class movimiento implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_movimiento_pk", nullable=false)
	private long id;
	
	@Column(name="tipo_movimiento", nullable=false)
	private String tipo_movimiento;
	
	@Column(name="fecha_movimiento", nullable=false)
	private String fecha_movimiento;
	
	@Column(name="cantidad_movimiento", nullable=false)
	private String cantidad_movimiento;
	
	@Column(name="precio_unidad_movimiento", nullable=false)
	private String precio_unidad_movimiento;
	
	@ManyToOne
	@JoinColumn(name="id_producto_pk", nullable=false)
	private producto producto;
	
	@ManyToOne
	@JoinColumn(name="id_usuario_pk", nullable=false)
	private usuario usuario;

	public movimiento() {
		super();
	}

	public movimiento(long id, String tipo_movimiento, String fecha_movimiento, String cantidad_movimiento,
			String precio_unidad_movimiento, co.edu.sena.gics.entities.producto producto,
			co.edu.sena.gics.entities.usuario usuario) {
		super();
		this.id = id;
		this.tipo_movimiento = tipo_movimiento;
		this.fecha_movimiento = fecha_movimiento;
		this.cantidad_movimiento = cantidad_movimiento;
		this.precio_unidad_movimiento = precio_unidad_movimiento;
		this.producto = producto;
		this.usuario = usuario;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipo_movimiento() {
		return tipo_movimiento;
	}

	public void setTipo_movimiento(String tipo_movimiento) {
		this.tipo_movimiento = tipo_movimiento;
	}

	public String getFecha_movimiento() {
		return fecha_movimiento;
	}

	public void setFecha_movimiento(String fecha_movimiento) {
		this.fecha_movimiento = fecha_movimiento;
	}

	public String getCantidad_movimiento() {
		return cantidad_movimiento;
	}

	public void setCantidad_movimiento(String cantidad_movimiento) {
		this.cantidad_movimiento = cantidad_movimiento;
	}

	public String getPrecio_unidad_movimiento() {
		return precio_unidad_movimiento;
	}

	public void setPrecio_unidad_movimiento(String precio_unidad_movimiento) {
		this.precio_unidad_movimiento = precio_unidad_movimiento;
	}

	public producto getProducto() {
		return producto;
	}

	public void setProducto(producto producto) {
		this.producto = producto;
	}

	public usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "movimiento [id=" + id + ", tipo_movimiento=" + tipo_movimiento + ", fecha_movimiento="
				+ fecha_movimiento + ", cantidad_movimiento=" + cantidad_movimiento + ", precio_unidad_movimiento="
				+ precio_unidad_movimiento + ", producto=" + producto + ", usuario=" + usuario + "]";
	}
	
	

}
