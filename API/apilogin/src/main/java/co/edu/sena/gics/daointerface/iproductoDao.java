package co.edu.sena.gics.daointerface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.producto;

public interface iproductoDao extends CrudRepository<producto, Long>{

}
