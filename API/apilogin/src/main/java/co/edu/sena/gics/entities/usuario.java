package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuario")

public class usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario_pk", nullable=false)
	private long id;
	
	@Column(name="nombre_usuario", nullable=false)
	private String nombre_usuario;
	
	@Column(name="contraseña", nullable=false)
	private String contraseña;
	
	@Column(name="documento", nullable=false)
	private String documento;

	public usuario() {
		super();
	}

	public usuario(long id, String nombre_usuario, String contraseña, String documento) {
		super();
		this.id = id;
		this.nombre_usuario = nombre_usuario;
		this.contraseña = contraseña;
		this.documento = documento;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Override
	public String toString() {
		return "Rol [id=" + id + ", nombre_usuario=" + nombre_usuario + ", contraseña=" + contraseña + ", documento="
				+ documento + "]";
	}
	
	
}
