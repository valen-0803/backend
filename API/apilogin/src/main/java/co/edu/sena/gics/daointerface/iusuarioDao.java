package co.edu.sena.gics.daointerface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.usuario;

public interface iusuarioDao extends CrudRepository<usuario, Long>{

}
