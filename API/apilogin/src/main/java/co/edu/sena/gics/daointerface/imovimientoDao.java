package co.edu.sena.gics.daointerface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.movimiento;

public interface imovimientoDao extends CrudRepository<movimiento, Long>{

}
