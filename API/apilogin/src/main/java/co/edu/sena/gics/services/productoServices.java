package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daointerface.iproductoDao;
import co.edu.sena.gics.entities.producto;
import co.edu.sena.gics.servicesinterface.iproductoServices;

@Service
public class productoServices implements iproductoServices{
	
	@Autowired
	private iproductoDao productoDao;

	@Override
	public List<producto> findAll() {
		return (List<producto>) productoDao.findAll();
	}

	@Override
	public producto create(producto producto) {
		return productoDao.save(producto);
	}

	@Override
	public producto update(long id, producto producto) {
		Optional<producto> pro = this.productoDao.findById(id);
		if(pro.isPresent()) {
		   producto.setId(pro.get().getId());
		return this.productoDao.save(producto);
		}
		else {
		return new producto();
		}
	}

	@Override
	public void delete(long id) {
		this.productoDao.deleteById(id);

	}


}
