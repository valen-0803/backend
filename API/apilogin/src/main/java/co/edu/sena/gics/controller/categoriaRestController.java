package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.categoria;
import co.edu.sena.gics.servicesinterface.icategoriaServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})
public class categoriaRestController {
	
	@Autowired
	private icategoriaServices categoriaServices;
	
	@GetMapping("/categoria")
	public List<categoria> getAll(){
		return categoriaServices.findAll();
	}
	
	@PostMapping("/categoria")
	public categoria create(@RequestBody categoria categoria) {
		System.out.println(categoria.toString());
		return categoriaServices.create(categoria);
	}
	
	@PutMapping("/categoria/{id}")
	public categoria update(@PathVariable int id,@RequestBody categoria categoria ){
	System.out.println(id);
	System.out.println(categoria);
	return this.categoriaServices.update(id,categoria);
	}

	@DeleteMapping("/categoria/{id}")
	    public void delete(@PathVariable int id) {
	this.categoriaServices.delete(id);

	}
	
}
