package co.edu.sena.gics.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="producto")

public class producto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_producto_pk", nullable=false)
	private long id;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="descripcion", nullable=false)
	private String descripcion;
	
	@Column(name="precio_costo", nullable=false)
	private String precio_costo;
	
	@Column(name="precio_venta", nullable=false)
	private String precio_venta;
	
	@Column(name="cantidad", nullable=false)
	private String cantidad;
	
	@Column(name="ganancia_costo_venta", nullable=false)
	private String ganancia_costo_venta;
	
	@ManyToOne
	@JoinColumn(name="id_categoria_pk", nullable=false)
	private categoria categoria;
	
	@ManyToOne
	@JoinColumn(name="id_estado_pk", nullable=false)
	private estado estado;
	
	@ManyToOne
	@JoinColumn(name="id_proveedores_pk", nullable=false)
	private proveedores proveedores;

	public producto() {
		super();
	}

	public producto(long id, String nombre, String descripcion, String precio_costo, String precio_venta,
			String cantidad, String ganancia_costo_venta, co.edu.sena.gics.entities.categoria categoria,
			co.edu.sena.gics.entities.estado estado, co.edu.sena.gics.entities.proveedores proveedores) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio_costo = precio_costo;
		this.precio_venta = precio_venta;
		this.cantidad = cantidad;
		this.ganancia_costo_venta = ganancia_costo_venta;
		this.categoria = categoria;
		this.estado = estado;
		this.proveedores = proveedores;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPrecio_costo() {
		return precio_costo;
	}

	public void setPrecio_costo(String precio_costo) {
		this.precio_costo = precio_costo;
	}

	public String getPrecio_venta() {
		return precio_venta;
	}

	public void setPrecio_venta(String precio_venta) {
		this.precio_venta = precio_venta;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getGanancia_costo_venta() {
		return ganancia_costo_venta;
	}

	public void setGanancia_costo_venta(String ganancia_costo_venta) {
		this.ganancia_costo_venta = ganancia_costo_venta;
	}

	public categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(categoria categoria) {
		this.categoria = categoria;
	}

	public estado getEstado() {
		return estado;
	}

	public void setEstado(estado estado) {
		this.estado = estado;
	}

	public proveedores getProveedores() {
		return proveedores;
	}

	public void setProveedores(proveedores proveedores) {
		this.proveedores = proveedores;
	}

	@Override
	public String toString() {
		return "producto [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio_costo="
				+ precio_costo + ", precio_venta=" + precio_venta + ", cantidad=" + cantidad + ", ganancia_costo_venta="
				+ ganancia_costo_venta + ", categoria=" + categoria + ", estado=" + estado + ", proveedores=" + proveedores
				+ "]";
	}

	
	
}
