package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.producto;
import co.edu.sena.gics.servicesinterface.iproductoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class productoRestController {
	
	@Autowired
	private iproductoServices productoServices;
	
	@GetMapping("/producto")
	public List<producto> getAll(){
		return productoServices.findAll();
	}
	
	@PostMapping("/producto")
	public producto create(@RequestBody producto producto) {
		System.out.println(producto.toString());
		return productoServices.create(producto);
	}
	
	@PutMapping("/producto/{id}")
	public producto update(@PathVariable int id,@RequestBody producto producto ){
	System.out.println(id);
	System.out.println(producto);
	return this.productoServices.update(id,producto);
	}

	@DeleteMapping("/producto/{id}")
	    public void delete(@PathVariable int id) {
	this.productoServices.delete(id);

	}

}
