package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.categoria;

public interface icategoriaServices {
	
	public List<categoria> findAll();
	
	public categoria create(categoria categoria);
	
	public categoria update(long id, categoria categoria);

	public void delete(long id);
	
}
