package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="proveedores")

public class proveedores implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_proveedores_pk", nullable=false)
	private long id;
	
	@Column(name="nombre_proveedor", nullable=false)
	private String nombre_proveedor;
	
	@Column(name="documento", nullable=false)
	private String documento;
	
	@Column(name="direccion", nullable=false)
	private String direccion;
	
	@Column(name="telefono", nullable=false)
	private String telefono;

	public proveedores() {
		super();
	}

	public proveedores(long id, String nombre_proveedor, String documento, String direccion, String telefono) {
		super();
		this.id = id;
		this.nombre_proveedor = nombre_proveedor;
		this.documento = documento;
		this.direccion = direccion;
		this.telefono = telefono;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre_proveedor() {
		return nombre_proveedor;
	}

	public void setNombre_proveedor(String nombre_proveedor) {
		this.nombre_proveedor = nombre_proveedor;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "proveedor [id=" + id + ", nombre_proveedor=" + nombre_proveedor + ", documento=" + documento
				+ ", direccion=" + direccion + ", telefono=" + telefono + "]";
	}
	
	

}
