package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daointerface.iusuarioDao;
import co.edu.sena.gics.entities.usuario;
import co.edu.sena.gics.servicesinterface.iusuarioServices;

@Service
public class usuarioServices implements iusuarioServices{
	
	@Autowired
	private iusuarioDao usuarioDao;

	@Override
	public List<usuario> findAll() {
		return (List<usuario>) usuarioDao.findAll();
	}

	@Override
	public usuario create(usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public usuario update(long id, usuario usuario) {
		Optional<usuario> us = this.usuarioDao.findById(id);
		if(us.isPresent()) {
		   usuario.setId(us.get().getId());
		return this.usuarioDao.save(usuario);
		}
		else {
		return new usuario();
		}
	}

	@Override
	public void delete(long id) {
		this.usuarioDao.deleteById(id);
	}


}
