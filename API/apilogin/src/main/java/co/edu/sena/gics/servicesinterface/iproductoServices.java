package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.producto;

public interface iproductoServices {
	
	public List<producto> findAll();
	
	public producto create(producto producto);
	
	public producto update(long id, producto producto);

	public void delete(long id);
	
}
