package co.edu.sena.gics.daointerface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.estado;

public interface iestadoDao extends CrudRepository<estado, Long>{

}
