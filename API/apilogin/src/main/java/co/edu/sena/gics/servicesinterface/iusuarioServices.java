package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.usuario;

public interface iusuarioServices {
	
	public List<usuario> findAll();
	
	public usuario create(usuario usuario);
	
	public usuario update(long id, usuario usuario);

	public void delete(long id);


}
