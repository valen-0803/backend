package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daointerface.iproveedoresDao;
import co.edu.sena.gics.entities.proveedores;
import co.edu.sena.gics.servicesinterface.iproveedoresServices;

@Service
public class proveedoresServices implements iproveedoresServices{
	
	@Autowired
	private iproveedoresDao proveedoresDao;

	@Override
	public List<proveedores> findAll() {
		return (List<proveedores>) proveedoresDao.findAll();
	}

	@Override
	public proveedores create(proveedores proveedores) {
		return proveedoresDao.save(proveedores);
	}

	@Override
	public proveedores update(long id, proveedores proveedores) {
		Optional<proveedores> prov = this.proveedoresDao.findById(id);
		if(prov.isPresent()) {
		   proveedores.setId(prov.get().getId());
		return this.proveedoresDao.save(proveedores);
		}
		else {
		return new proveedores();
		}
	}

	@Override
	public void delete(long id) {
		this.proveedoresDao.deleteById(id);

	}


}
