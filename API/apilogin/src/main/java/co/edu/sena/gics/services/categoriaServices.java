package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daointerface.icategoriaDao;
import co.edu.sena.gics.entities.categoria;
import co.edu.sena.gics.servicesinterface.icategoriaServices;

@Service
public class categoriaServices implements icategoriaServices{
	
	@Autowired
	private icategoriaDao categoriaDao;

	@Override
	public List<categoria> findAll() {
		return (List<categoria>) categoriaDao.findAll();
	}

	@Override
	public categoria create(categoria categoria) {
		return categoriaDao.save(categoria);
	}

	@Override
	public categoria update(long id, categoria categoria) {
		Optional<categoria> cat = this.categoriaDao.findById(id);
		if(cat.isPresent()) {
		   categoria.setId(cat.get().getId());
		return this.categoriaDao.save(categoria);
		}
		else {
		return new categoria();
		}
	}

	@Override
	public void delete(long id) {
		this.categoriaDao.deleteById(id);

	}



}
