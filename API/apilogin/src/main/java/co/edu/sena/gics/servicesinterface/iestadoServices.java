package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.estado;

public interface iestadoServices {
	
	public List<estado> findAll();
	
	public estado create(estado estado);
	
	public estado update(long id, estado estado);

	public void delete(long id);
	

}
