package co.edu.sena.gics.servicesinterface;

import java.util.List;

import co.edu.sena.gics.entities.proveedores;

public interface iproveedoresServices {
	
	public List<proveedores> findAll();
	
	public proveedores create(proveedores proveedores);
	
	public proveedores update(long id, proveedores proveedores);

	public void delete(long id);
	

}
