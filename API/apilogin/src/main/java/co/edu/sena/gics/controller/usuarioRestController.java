package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.usuario;
import co.edu.sena.gics.servicesinterface.iusuarioServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class usuarioRestController {
	
	@Autowired
	private iusuarioServices usuarioServices;
	
	@GetMapping("/usuario")
	public List<usuario> getAll(){
		return usuarioServices.findAll();
	}
	
	@PostMapping("/usuario")
	public usuario create(@RequestBody usuario usuario) {
		System.out.println(usuario.toString());
		return usuarioServices.create(usuario);
	}
	
	@PutMapping("/usuario/{id}")
	public usuario update(@PathVariable int id,@RequestBody usuario usuario ){
	System.out.println(id);
	System.out.println(usuario);
	return this.usuarioServices.update(id,usuario);
	}

	@DeleteMapping("/usuario/{id}")
	    public void delete(@PathVariable int id) {
	this.usuarioServices.delete(id);

	}
}
