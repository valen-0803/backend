package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daointerface.iestadoDao;
import co.edu.sena.gics.entities.estado;
import co.edu.sena.gics.servicesinterface.iestadoServices;

@Service
public class estadoServices implements iestadoServices{
	
	@Autowired
	private iestadoDao estadoDao;
	
	@Override
	public List<estado> findAll() {
		return (List<estado>) estadoDao.findAll();
	}

	@Override
	public estado create(estado estado) {
		return estadoDao.save(estado);
	}

	@Override
	public estado update(long id, estado estado) {
		Optional<estado> es = this.estadoDao.findById(id);
		if(es.isPresent()) {
		   estado.setId(es.get().getId());
		return this.estadoDao.save(estado);
		}
		else {
		return new estado();
		}
	}

	@Override
	public void delete(long id) {
		this.estadoDao.deleteById(id);

	}


}
